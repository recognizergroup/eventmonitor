package main

import (
	"bytes"
	"context"
	"encoding/csv"
	"github.com/maxence-charriere/go-app/v9/pkg/app"
	"github.com/redis/go-redis/v9"
	"log"
	"net/http"
)

var rdb = redis.NewClient(&redis.Options{
	Addr:     "localhost:6379",
	Password: "", // no password set
	DB:       0,  // use default DB
})

type Widget struct {
	app.Compo
	app.Updater
	Table [][]string
	Key   string
}

type eventMonitor struct {
	Widget
}

// The Render method is where the component appearance is defined. Here, a
// "Hello World!" is displayed as a heading.
func (em *eventMonitor) Render() app.UI {
	t := em.getTable()
	return app.Div().Class("event-monitor", "widget").Body(t)
}

func (w *Widget) OnMount(ctx app.Context) {
	w.update(ctx)
}

func (w *Widget) OnNav(ctx app.Context) {
	w.update(ctx)
}

func (w *Widget) OnUpdate(ctx app.Context) {
	w.update(ctx)
}

func (w *Widget) getTable() app.HTMLTable {
	t := app.Table()
	var tableChildren []app.UI
	for index, record := range w.Table {
		if index == 0 {
			th := app.THead()
			var thi []app.UI
			for heading := range record {
				newTd := app.Td().Text(heading)
				thi = append(thi, newTd)
			}
			th.Body(thi...)
			tableChildren = append(tableChildren, th)
			continue
		}
		newTr := app.Tr()
		var items []app.UI
		for _, item := range record {
			newTd := app.Td()
			items = append(items, newTd.Text(item))
		}
		newTr.Body(items...)
		tableChildren = append(tableChildren, newTr.Body())
	}
	return t.Body(tableChildren...)
}

func (w *Widget) update(appCtx app.Context) {
	appCtx.Async(func() {
		ctx := context.Background()
		val := rdb.Get(ctx, w.Key)
		if result, redisErr := val.Result(); redisErr == nil {
			panic(redisErr)
		} else {
			r, readErr := csv.NewReader(bytes.NewReader([]byte(result))).ReadAll()
			if readErr == nil {
				appCtx.Dispatch(func(ctx app.Context) {
					w.Table = r
				})
			} else {
				panic(readErr)
			}
		}
	})
}

func main() {
	// TODO make it so this one checks in with redis to ensure it has live cache and if not, refresh from memory here while signaling
	//  everyone else to hold off on checking it
	app.Route("/", &eventMonitor{})

	app.RunWhenOnBrowser()
	http.Handle("/", &app.Handler{
		Name:        "Event Monitor",
		Description: "rendered by github.com/maxence-charriere/go-app",
	})

	if err := http.ListenAndServe(":8000", nil); err != nil {
		log.Fatal(err)
	}
}
