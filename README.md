# EventMonitor
A web GUI for querying and viewing telemetry traces
 collected from instrumented systems-under-test (SUT).
## Purpose
Existing service monitoring tools, cloud or not, over-rely on logs or metrics,
when what we actually need are recordings of the inputs that caused the issue.
And we need a way to visualize the recordings so that searching and viewing
are intuitive and efficient. EventMonitor offers the following features to 
solve this need.
## Features
### Composability
Movable, configurable widgets to present/highlight data differently while
literally drawing a visual connection between correlated data that is labeled
with whatever transform is being applied (other than enlarge).
These transforms are actually redis JSON queries on the back-end.
All widgets derive from components designed for re-use and easy styling.
### Responsiveness
Widget data views should have small/medium/large screen versions that show
or less nested information e.g. a Trace on a big screen might show the Spans
within. Similarly, on hover, elements should pop out to their more detailed 
visualization.
### Data Views
The following are hierarchical units, each of which has its own responsive and 
composable data visualization.
#### Case
A set of all TraceRecords with the same hash value. A case is opened when a 
TraceRecord hash is seen for the first time. Consequently, tests should be
developed to replicate. When a bugfix for the case passes those tests, the
case should be closed. It can be re-opened on regression.
#### Incident
A set of TraceRecords with the same hash value and Timestamps that fall
between the opening or closing of a case. An Incident that is a reoccurrence
of a previous Incident is a regression. Incidents can represent any grouping
desired by the user. Examples:
* Instances across multiple or single deployments of same service
* Instances with a common dependency
* Instances with a specific error response
#### Instance
An individual TraceRecord with its own unique Timestamp. It contains a reduced
(anonymized) recording of the System-Under-Test I/O.
### Default Widgets
#### Span
* Search
* Present
#### Trace
* Search
* Present
#### Alarm
* Set on schedule or...
* triggered by widget actions
* triggered by absence of action by a given time or other action
* disarmed by job reporting in
* disarmed by human operator
* disarmed by next alarm of same type
